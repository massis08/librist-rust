#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use libc::{
    FILE,
    size_t,
};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));


#[cfg(test)]
mod tests {
    use crate::*;
    use std::io::stderr;
    use std::ffi::CString;
    use std::os::unix::io::AsRawFd;

    #[test]
    fn smoke() {
        unsafe {
            let mode = CString::new("w").unwrap();
            let stderr = libc::fdopen(stderr().as_raw_fd(), mode.as_ptr());
            let mut logging_settings: *mut rist_logging_settings = std::ptr::null_mut();
            let res = rist_logging_set(
                &mut logging_settings,
                rist_log_level_RIST_LOG_DEBUG,
                None,
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                stderr,
            );
            assert_eq!(res, 0, "Failed to setup logging!");

            let mut ctx: *mut rist_ctx = std::ptr::null_mut();
            let res = rist_receiver_create(
                &mut ctx,
                rist_profile_RIST_PROFILE_MAIN,
                logging_settings,
            );
            assert_eq!(res, 0, "Could not create rist receiver context");

            let res = rist_start(ctx);
            assert_eq!(res, 0, "Could not start rist receiver");

            let res = rist_destroy(ctx);
            assert_eq!(res, 0, "Could not destroy rist receiver context");

            libc::free(logging_settings as _);
            libc::fclose(stderr);
        }
    }
}
